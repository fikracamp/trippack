import React, {Component} from 'react';
import {withFirebase} from '../Firebase';

class OffersPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            offers: [],
            offers2: [
                {
                    'name': 'name1',
                    'date': '2019-10-1'
                },
                {
                    'name': 'name2',
                    'date': '2018-10-1'
                }
            ]
        };
        this.props.firebase.fb.collection('offers').where('qty', '>', 0)
            .onSnapshot((snapshot) => {
                let offers = [];
                snapshot.forEach((doc) => {
                    offers.push(doc.data());
                });
                this.setState({offers: offers})
            });
    }

    render() {
        const {offers, offers2} = this.state;

        // console.log('offers render', offers);
        // console.log('offers2 render', offers2);

        return (
            <div>
                <h1 className="py-2">All Available Offers</h1>
                <OfferList offers={offers}/>
            </div>
        );
    }
}

const OfferList = (props) => {
    return (
        <div className="d-flex flex-row justify-content-start flex-wrap ">
            {
                props.offers.map((offer, i) => {
                    // var date = new Date(offer.date.seconds);
                    return (
                        <div className=" d-flex align-items-start m-2 offer-card" key={i}>
                            <div className="card">
                                <img className="card-img-top"
                                     src='https://images.unsplash.com/photo-1519999482648-25049ddd37b1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'
                                     alt="Card image cap"/>
                                <div className="card-body">
                                    <h5 className="card-title">{offer.name}</h5>
                                    <h6>2019-10-1</h6>
                                    <p className="card-text">Some quick example text to build on the card title and make
                                        up the
                                        bulk of the card's content.</p>
                                    <a href="#" className="btn btn-outline-info btn-block">Book</a>
                                </div>
                            </div>
                        </div>

                    )
                })
            }
        </div>
    )
};

export default withFirebase(OffersPage);
