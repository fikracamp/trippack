import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {compose} from 'recompose';
import {SignUpLink} from '../SignUp';
import {PasswordForgetLink} from '../PasswordForget';
import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';

const SignInPage = () => (
    <div className="text-center">
        <div className="sign">
            <SignInForm/>
        </div>
        <PasswordForgetLink/>
        <SignUpLink/>
    </div>
);

const INITIAL_STATE = {
    email: '',
    password: '',
    error: null,
};

class SignInFormBase extends Component {
    constructor(props) {
        super(props);

        this.state = {...INITIAL_STATE};
    }

    onSubmit = event => {
        const {email, password} = this.state;

        this.props.firebase
            .doSignInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({...INITIAL_STATE});
                this.props.history.push(ROUTES.HOME);
            })
            .catch(error => {
                this.setState({error});
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        const {email, password, error} = this.state;

        const isInvalid = password === '' || email === '';

        return (
            <form className="form-signin" onSubmit={this.onSubmit}>
                <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
                <label htmlFor="inputEmail" className="sr-only">Email address</label>
                <input
                    id="inputEmail"
                    className="form-control"
                    name="email"
                    value={email}
                    onChange={this.onChange}
                    type="email"
                    placeholder="Email Address"
                    required=""
                    autoFocus=""
                />
                <label htmlFor="inputPassword" className="sr-only">Password</label>
                <input
                    name="password"
                    id="inputPassword"
                    className="form-control"
                    value={password}
                    onChange={this.onChange}
                    type="password"
                    placeholder="Password"
                    required=""
                />
                <button disabled={isInvalid} className="btn btn-lg btn-info btn-block  mb-4 my-4" type="submit">
                    Sign In
                </button>

                {error && <div className="alert alert-danger" role="alert">{error.message}</div>}
            </form>
        );
    }
}

const SignInForm = compose(
    withRouter,
    withFirebase,
)(SignInFormBase);

export default SignInPage;

export {SignInForm};
