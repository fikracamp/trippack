import React from 'react';

import { AuthUserContext } from '../Session';
import {PasswordForgetForm, PasswordForgetLink} from '../PasswordForget';
import PasswordChangeForm from '../PasswordChange';
import { withAuthorization } from '../Session';

const AccountPage = () => (
  <AuthUserContext.Consumer>
    {authUser => (
      <div>
        <h4 className="h4 my-4">Your E-mail: {authUser.email}</h4>
        {/*<PasswordForgetForm />*/}
        <PasswordForgetLink/>
        <PasswordChangeForm />
      </div>
    )}
  </AuthUserContext.Consumer>
);

const authCondition = authUser => !!authUser;

export default withAuthorization(authCondition)(AccountPage);
