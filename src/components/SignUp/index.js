import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {compose} from 'recompose';

import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';

const SignUpPage = () => (
    <div className="text-center">
        <div className="sign">
            <SignUpForm/>
        </div>
    </div>
);

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

class SignUpFormBase extends Component {
    constructor(props) {
        super(props);

        this.state = {...INITIAL_STATE};
    }

    onSubmit = event => {
        const {username, email, passwordOne} = this.state;

        this.props.firebase
            .doCreateUserWithEmailAndPassword(email, passwordOne)
            .then(authUser => {
                // Create a user in your Firebase realtime database
                this.props.firebase
                    .user(authUser.user.uid)
                    .set({
                        username,
                        email,
                    })
                    .then(() => {
                        this.setState({...INITIAL_STATE});
                        this.props.history.push(ROUTES.HOME);
                    })
                    .catch(error => {
                        this.setState({error});
                    });
            })
            .catch(error => {
                this.setState({error});
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            error,
        } = this.state;

        const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === '' ||
            email === '' ||
            username === '';

        return (
            <form className="form-signup" onSubmit={this.onSubmit}>
                <h1 className="h3 mb-3 font-weight-normal">Sign up</h1>
                <label htmlFor="inputEmail" className="sr-only">Email address</label>
                <input
                    id="inputEmail"
                    name="email"
                    className="form-control"
                    value={email}
                    onChange={this.onChange}
                    type="email"
                    placeholder="Email Address"
                    required=""
                    autoFocus=""
                />
                <label htmlFor="username" className="sr-only">Full Name</label>
                <input
                    id="username"
                    name="username"
                    value={username}
                    className="form-control"
                    onChange={this.onChange}
                    type="text"
                    placeholder="Full Name"
                    required=""
                    autoFocus=""
                />
                <label htmlFor="passwordOne" className="sr-only">Password</label>
                <input
                    id="passwordOne"
                    name="passwordOne"
                    value={passwordOne}
                    className="form-control"
                    onChange={this.onChange}
                    type="password"
                    placeholder="Password"
                    required=""
                    autoFocus=""
                />
                <label htmlFor="passwordTwo" className="sr-only">Password</label>
                <input
                    id="passwordTwo"
                    name="passwordTwo"
                    value={passwordTwo}
                    className="form-control"
                    onChange={this.onChange}
                    type="password"
                    placeholder="Confirm Password"
                    required=""
                    autoFocus=""
                />
                <button className="btn btn-lg btn-success btn-block mb-4 my-4" disabled={isInvalid} type="submit">
                    Sign Up
                </button>

                {error && <div className="alert alert-danger" role="alert">{error.message}</div>}
            </form>
        );
    }
}

const SignUpLink = () => (
    <p>
        Don't have an account? <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
    </p>
);

const SignUpForm = withRouter(withFirebase(SignUpFormBase));
// const SignUpForm = compose(
//     withRouter,
//     withFirebase,
// )(SignUpFormBase);

export default SignUpPage;

export {SignUpForm, SignUpLink};
