import React from 'react';
import {Link} from 'react-router-dom';

import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';

import {AuthUserContext} from '../Session';

const Navigation = () => (
    <div>
        <AuthUserContext.Consumer>
            {authUser =>
                authUser ? <NavigationAuth/> : <NavigationNonAuth/>
            }
        </AuthUserContext.Consumer>
    </div>
);

const NavigationAuth = () => (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to={ROUTES.LANDING}>TripPack</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                    <Link className="nav-link" to={ROUTES.HOME}>Home</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to={ROUTES.ADMIN}>Admin</Link>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Profile
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <Link className="dropdown-item" to={ROUTES.ACCOUNT}>Account</Link>
                        <Link className="dropdown-item" to={ROUTES.PASSWORD_FORGET}>Forget Password</Link>
                        {/*<Link className="dropdown-item" to={ROUTES.PASSWORD_CHANGE}>Change Password</Link>*/}
                    </div>
                </li>
            </ul>

            <SignOutButton/>
        </div>
    </nav>
);

const NavigationNonAuth = () => (
    <nav className="navbar navbar-light bg-light justify-content-between">
        <Link className="navbar-brand" to={ROUTES.LANDING}>TripPack</Link>
        <Link className="btn btn-info" to={ROUTES.SIGN_IN}>Sign In</Link>
    </nav>
);

export default Navigation;
