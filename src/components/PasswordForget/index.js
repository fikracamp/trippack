import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import {withFirebase} from '../Firebase';
import * as ROUTES from '../../constants/routes';

const PasswordForgetPage = () => (
    <div className="text-center">
        <div className="sign">
            <PasswordForgetForm/>
        </div>
    </div>
);

const INITIAL_STATE = {
    email: '',
    error: null,
};

class PasswordForgetFormBase extends Component {
    constructor(props) {
        super(props);

        this.state = {...INITIAL_STATE};
    }

    onSubmit = event => {
        const {email} = this.state;

        this.props.firebase
            .doPasswordReset(email)
            .then(() => {
                this.setState({...INITIAL_STATE});
            })
            .catch(error => {
                this.setState({error});
            });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        const {email, error} = this.state;

        const isInvalid = email === '';

        return (
            <form className="form-forgetpass" onSubmit={this.onSubmit}>
                <h1 className="h3 mb-3 font-weight-normal">Password Forget?</h1>
                <label htmlFor="inputEmail" className="sr-only">Email address</label>
                <input
                    name="email"
                    id="inputEmail"
                    className="form-control"
                    value={this.state.email}
                    onChange={this.onChange}
                    type="email"
                    placeholder="Email Address"
                    required=""
                    autoFocus=""
                />
                <button className="btn btn-lg btn-warning btn-block mb-4 my-4" disabled={isInvalid} type="submit">
                    Reset My Password
                </button>

                {error && <div className="alert alert-danger" role="alert">{error.message}</div>}
            </form>
        );
    }
}

const PasswordForgetLink = () => (
    <p>
        <Link to={ROUTES.PASSWORD_FORGET}>Forgot Password?</Link>
    </p>
);

export default PasswordForgetPage;

const PasswordForgetForm = withFirebase(PasswordForgetFormBase);

export {PasswordForgetForm, PasswordForgetLink};
